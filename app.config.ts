export default defineAppConfig({
    ui: {
      avatar: {
        rounded: 'object-cover'
      },
      notifications: {
        // Show toasts at the top right of the screen
        position: 'top-0 bottom-auto'
      }
    }
  })