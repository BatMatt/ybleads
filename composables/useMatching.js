export default function useMatching() {
    const supabase = useSupabaseClient();
    const user = useSupabaseUser();

    const loading = ref(false);
    const contacts = ref([]);
    const properties = ref([]);
    const matches = ref([]);

    const fetchProperties = async () => {
        loading.value = true;
        const { data, error } = await supabase.from('properties').select('*, contacts!properties_current_owner_fkey(*)');
        if (error) {
            console.error('Error fetching properties:', error);
        } else {
            properties.value = data;
        }
        loading.value = false;
    };

    const fetchContacts = async () => {
        loading.value = true;
        const { data, error } = await supabase.from('contacts').select('*').not('sector', 'is.null');
        if (error) {
            console.error('Error fetching contacts:', error);
        } else {
            contacts.value = data;
        }
        loading.value = false;
    };

    const calculateDistance = (lat1, lng1, lat2, lng2) => {
        const R = 6371; // Rayon de la Terre en kilomètres
        const dLat = (lat2 - lat1) * Math.PI / 180;
        const dLng = (lng2 - lng1) * Math.PI / 180;
        const a =
            0.5 - Math.cos(dLat) / 2 +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            (1 - Math.cos(dLng)) / 2;

        return R * 2 * Math.asin(Math.sqrt(a));
    };

    const matchContactsAndProperties = async () => {
        const matchResults = [];

        contacts.value.forEach(contact => {
            properties.value.forEach(property => {

                // Vérification du budget
                if (property.status !== PropertyStatus.ESTIMATION) {
                    const price = property.price;
                    if (price > contact.budget_max) {
                        return; // Prochain bien immobilier
                    }
                } else {
                  
                    const min = property.estimated_min_price;
                    const max = property.estimated_max_price;

                    if (contact.budget_max < min || contact.budget_max > max) {
                        return;
                    }
                }

                // Vérification des caractéristiques du bien
                if (contact.bedrooms && property.bedrooms < contact.bedrooms) { 
                    return // Prochain bien immobilier
                }

                if (contact.rooms && property.rooms < contact.rooms) {
                    return // Prochain bien immobilier
                }

                if (contact.area_sq_m && property.area_sq_m < contact.area_sq_m) {
                    return // Prochain bien immobilier
                }

                // Vérification du type de propriété
                if (!contact.property_type.includes(property.property_type)) {
                    return; // Prochain bien immobilier
                }

                // Vérification de la localisation
                const contactLocation = contact.sector.value;
                const propertyLocation = property.address.value;
                const distance = calculateDistance(contactLocation.lat, contactLocation.lng, propertyLocation.lat, propertyLocation.lng);

                if (distance > contact.search_radius_km) {
                    return; // Prochain bien immobilier
                }

                // Ajout au tableau des matches
                matchResults.push({ contact_id: contact.id, property_id: property.id });
            });
        });

        await supabase.rpc('truncate_contacts_properties')

        const { data, error } = await supabase.from('contacts_properties')
            .upsert(matchResults, { onConflict: ['contact_id', 'property_id'] })
            .select('*, contacts(*), properties(*)');

        if (error) {
            console.error('Une erreur est survenue lors du rapprochement', error);
        }

        matches.value = data;
    };

    const initializeMatching = async () => {
        await fetchProperties();
        await fetchContacts();
        await matchContactsAndProperties();
    };

    return {
        initializeMatching
    };
}
