import { ref, provide, inject } from 'vue'

const contactSlideoverKey = Symbol()

export function useContactSlideoverProvider() {
  const isOpen = ref(false)
  const contactId = ref(null)
  const contact = ref(null)
  const isLoading = ref(false)
  const supabase = useSupabaseClient()

  async function openSlideover(id) {
    contactId.value = id
    isOpen.value = true
    await fetchContact()
  }

  function closeSlideover() {
    isOpen.value = false
    contact.value = null
  }

  async function fetchContact() {
    if (contactId.value) {
      isLoading.value = true
      try {
        const { data, error } = await supabase
          .from('contacts')
          .select('*, countries(id, name), contacts_jobs(*, jobs(*)), contacts_properties(*, properties(*))')
          .eq('id', contactId.value)
          .single()

        if (error) {
          console.error('Error fetching contact:', error)
        } else {
          contact.value = data
        }
      } finally {
        isLoading.value = false
      }
    }
  }

  provide(contactSlideoverKey, {
    isOpen,
    contact,
    isLoading,
    openSlideover,
    closeSlideover,
  })
}

export function useContactSlideover() {
  return inject(contactSlideoverKey)
}