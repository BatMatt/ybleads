export const useRealtimeState = () => {
  const supabase = useSupabaseClient()
  const user = useSupabaseUser()
  const feeSchedules = useState('feeSchedules', () => [])
  const jobs = useState('jobs', () => [])
  const countries = useState('countries', () => [])
  const contactsProperties = useState('contactsProperties', () => [])
  const userDetails = useState('userDetails', () => null)
  const channels = []

  const fetchData = async () => {
    const [feeSchedulesData, jobsData, countriesData, contactsPropertiesData, userDetailsData] = await Promise.all([
      supabase.from('fee_schedules').select('*').eq('user_id', user.value.id),
      supabase.from('jobs').select('*'),
      supabase.from('countries').select('*'),
      supabase.from('contacts_properties').select('*'),
      supabase.from('user_details').select(`
        *,
        countries(*),
        team:teams!teams_manager_id_fkey (
      id,
      name,
      country,
      members:user_teams (
        id,
        team_id,
        building_session,
        hierarchy_level,
        position,
        notes,
        profile,
        status,
        first_name,
        last_name,
        identifier,
        avatar,
        birth_date,
        phone,
        email,
        address,
        country_id
      )
    )
      `).eq('user_id', user.value.id).single()
    ])

    feeSchedules.value = feeSchedulesData.data || []
    jobs.value = jobsData.data || []
    countries.value = countriesData.data || []
    contactsProperties.value = contactsPropertiesData.data || []
    userDetails.value = userDetailsData.data || null
  }

  const setupRealtimeListeners = () => {
    const tables = ['fee_schedules', 'jobs', 'countries', 'contacts_properties', 'user_details']

    tables.forEach(table => {
      const channel = supabase.channel(`public:${table}`)
        .on('postgres_changes', { event: '*', schema: 'public', table }, () => {
          fetchData()
        })
        .subscribe()

      channels.push(channel)
    })
  }

  const cleanup = () => {
    channels.forEach(channel => {
      supabase.removeChannel(channel)
    })
  }

  return {
    feeSchedules,
    jobs,
    countries,
    contactsProperties,
    userDetails,
    fetchData,
    setupRealtimeListeners,
    cleanup
  }
}