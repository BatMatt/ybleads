export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.provide('openGoogleMaps', (lat, lng) => {
      let coordinates;
  
      // Si lat est un string, on suppose qu'il contient les coordonnées
      if (typeof lat === 'string') {
        const parts = lat.split(',').map(part => part.trim());
        if (parts.length !== 2) {
          throw new Error('La chaîne de caractères doit contenir deux valeurs séparées par une virgule');
        }
        coordinates = parts.map(coord => parseFloat(coord));
      } else if (typeof lat === 'number' && typeof lng === 'number') {
        coordinates = [lat, lng];
      } else {
        throw new Error('Les coordonnées doivent être des nombres ou une chaîne de caractères au format "lat, lng"');
      }
  
      const [latitude, longitude] = coordinates;
  
      if (isNaN(latitude) || isNaN(longitude)) {
        throw new Error('Les coordonnées doivent être des nombres valides');
      }
  
      const url = `https://www.google.com/maps?q=${latitude},${longitude}`;
      window.open(url, '_blank');
    });
  });
  