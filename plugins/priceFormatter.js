export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.provide('formatPrice', (price) => {
      if (typeof price !== 'number') {
        throw new Error('Le prix doit être un nombre');
      }
      return new Intl.NumberFormat('fr-FR', {
        style: 'currency',
        currency: 'EUR'
      }).format(price);
    });
  });