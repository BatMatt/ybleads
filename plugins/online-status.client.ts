// plugins/online-status.client.ts
import { useOnline } from '@vueuse/core'
import { watch } from 'vue'

export default defineNuxtPlugin((nuxtApp) => {
  const online = useOnline()
  const router = useRouter()

  watch(online, (isOnline) => {
    if (!isOnline) {
      router.push('/offline')
    } else if (router.currentRoute.value.path === '/offline') {
      router.push('/')
    }
  })
})