import placekit from '@placekit/client-js/lite'

export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig()
  const pk = placekit(config.public['placeKitApiKey'], {
    countries: ['fr'],
    language: 'fr',
    maxResults: 10,
  })

  nuxtApp.provide('placekit', pk)
})
