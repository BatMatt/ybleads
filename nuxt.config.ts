export default defineNuxtConfig({
  app: {
    head: {
      title: 'YB Leads',
      viewport: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no',
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        // Vous pouvez ajouter d'autres formats d'icônes ici
        { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
        { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
        { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      ]
    }
  },
  runtimeConfig: {
    public: {
      placeKitApiKey: process.env.PLACEKIT_KEY
    }
  },
  devtools: { enabled: true },
  modules: ["@nuxt/ui", "@nuxtjs/supabase", "@vueuse/nuxt"],
  buildModules: [
    '@nuxtjs/date-fns'
  ],
  supabase: {
    redirect: true,
    redirectOptions: {
      login: '/login',
      callback: '',
      include: undefined,
      exclude: ['/register'],
      cookieRedirect: false,
    }
  },
  dateFns: {
    defaultLocale: 'fr'
  }
})