export function prepareSMS(row, sms_template) {
    const encodedMessage = encodeURIComponent('Bonjour MR/MME ' + row.first_name + ' ' + row.last_name + ', ' + sms_template);
    const smsLink = `sms:${row.phone}?body=${encodedMessage}`;
    window.open(smsLink, '_blank');
}