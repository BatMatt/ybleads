export enum ContactType {
    PROSPECT = "Prospect",
    ACQUEREUR = "Acquéreur",
    VENDEUR = "Vendeur",
    PARTENAIRE = "Partenaire"
}

export enum PropertyType {
    APPARTEMENT = "Appartement",
    MAISON = "Maison",
    FONDS_DE_COMMERCE = "Fonds de commerce",
    TERRAIN = "Terrain"
}

export enum PropertyStatus {
    ESTIMATION = "Estimation",
    A_VENDRE = "A vendre",
    SOUS_COMPROMIS = "Sous compromis",
    VENDU = "Vendu",
    NON_VENDU = "Non vendu"
}

export enum FeeType {
    FIXE = "Fixe",
    POURCENTAGE = "Pourcentage"
}

export enum ProspectionStatus {
    VENTE_PAR_CONFRERE = "Vente par un confrère",
    CONTACT_ETABLI = "Contact établi",
    RDV_PROGRAMME = "Rendez-vous programmé",
    OFFRE_PRESENTEE = "Offre présentée",
    SOUS_MANDAT = "Sous mandat",
    PROSPECT_INTERESSE = "Prospect intéressé",
    PROSPECT_NON_INTERESSE = "Prospect non intéressé",
    A_RECONTACTER = "A recontacter"
}

export enum SuggestionCategory {
    BUG = "Bug",
    AMELIORATION = "Amélioration",
    NOUVELLE_FONCTIONNALITE = "Nouvelle fonctionnalité"
}

export enum UserTeamProfile {
    BLUE = "Bleu",
    YELLOW = "Jaune",
    RED = "Rouge",
    GREEN = "Vert"
}

export enum UserTeamStatus {
    TODO = "A faire",
    PENDING = "En cours",
    DONE = "Effectué"
}